var express = require('express');
var http = require('http');
var https = require('https');

var router = express.Router();

var amadeusApiKey = "MTBCRay1xLJgmeAP21Au9IBVovvGHNR6";

/* GET flights list. */
router.get('/:origin/:destination/:departDate', function(req, res, next) {

    var origin = req.params.origin;
    var destination = req.params.destination;
    // var departDateRaw = req.params.departDate;

    // var departDateParsed = new Date(parseFloat(departDateRaw));
    // var departDate = departDateParsed.getFullYear()+"-"+(departDateParsed.getMonth() + 1)+"-"+departDateParsed.getDate();

    var date = new Date(req.params.departDate);
    var dd = date.getDate();
    var mm = date.getMonth()+1; //January is 0!
    var yyyy = date.getFullYear();

    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    var date = yyyy+'-'+mm+'-'+dd;

    return https.get({
        host: 'api.sandbox.amadeus.com',
        path: '/v1.2/flights/low-fare-search?apikey=' + amadeusApiKey + '&currency=AUD&origin=' + origin + '&destination=' + destination + '&departure_date=' + date
    }, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        // Data reception is done, do whatever with it!
        response.on('end', function() {
            try{
                var parsed = JSON.parse(body);
                var resultData = [];

                // For each itineraries in result
                for(var result in parsed.results){
                    if (parsed.results.hasOwnProperty(result)) {
                        var resultObj = parsed.results[result];
                        var resultRes = {
                            itineraries: [],
                            fare: resultObj.fare.total_price
                        };

                        // For each itinerary in itineraries
                        for(var itinerary in resultObj.itineraries){
                            if (resultObj.itineraries.hasOwnProperty(itinerary)) {
                                var itineraryObj = resultObj.itineraries[itinerary];
                                var itineraryRes = {
                                    flights:[]
                                };

                                // For each flight in the itinerary
                                for(var flight in itineraryObj.outbound.flights){
                                    if (itineraryObj.outbound.flights.hasOwnProperty(flight)) {
                                        var flightObj = itineraryObj.outbound.flights[flight];
                                        var flightRes = {
                                            origin: flightObj.origin.airport,
                                            destination: flightObj.destination.airport,
                                            departDate: flightObj.departs_at,
                                            arriveDate: flightObj.arrives_at,
                                            airline: flightObj.operating_airline,
                                            class: flightObj.booking_info.travel_class
                                        }
                                        itineraryRes.flights.push(flightRes);
                                    }
                                }

                                resultRes.itineraries.push(itineraryRes);
                            }
                        }
                        resultData.push(resultRes);
                    }
                }

                res.contentType('application/json');
                res.send(JSON.stringify(resultData));
            }
            catch (err) {
                return next(err);
            }
        });
        response.on('error', function (e) {
            console.log("error connecting" + e.message);
        });
    });
});

module.exports = router;
