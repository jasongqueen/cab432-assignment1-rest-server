var express = require('express');
var http = require('http');
var https = require('https');

var router = express.Router();

var googleApiKey = "AIzaSyBnZeAoQmaWTIB7179OdGU-yoACCd5GFa4";
var amadeusApiKey = "MTBCRay1xLJgmeAP21Au9IBVovvGHNR6";

/* GET destinations listing. */
router.get('/search/:substring', function(req, res, next) {

  var substring = req.params.substring;
  substring = encodeURI(substring);

  return https.get({
    host: 'maps.googleapis.com',
    path: '/maps/api/place/autocomplete/json?input='+substring+'&language=en-AU&key=' + googleApiKey
  }, function(response) {
    // Continuously update stream with data
    var body = '';
    response.on('data', function(d) {
      body += d;
    });
    // Data reception is done, do whatever with it!
    response.on('end', function() {
      try {
          var parsed = JSON.parse(body);

          var result = [];

          for(var prediction in parsed.predictions){
            if (parsed.predictions.hasOwnProperty(prediction)) {
              var city = parsed.predictions[prediction]
              result.push({
                fullName: city.description, placeId: city.place_id});
            }
          }

          res.contentType('application/json');
          res.send(JSON.stringify(result));
      }
      catch (err) {
        return next(err);
      }
    });

    response.on('error', function (e) {
      console.log("error connecting" + e.message);
    });
  });

});

/* GET destination details. */
router.get('/details/:placeId', function(req, res, next) {

  var placeId = req.params.placeId;
  placeId = encodeURI(placeId);

  return https.get({
    host: 'maps.googleapis.com',
    path: '/maps/api/place/details/json?placeid='+placeId+'&key=' + googleApiKey
  }, function(response) {
    // Continuously update stream with data
    var body = '';
    response.on('data', function(d) {
      body += d;
    });
    // Data reception is done, do whatever with it!
    response.on('end', function() {
      try{
        var parsed = JSON.parse(body);
        var city = '';
        var state = '';
        var country = '';



        for(var address_component in parsed.result.address_components){
          if (parsed.result.address_components.hasOwnProperty(address_component)) {
            var address = parsed.result.address_components[address_component];
            var type = address.types[0];
            var data = address.long_name;

            if (type == 'locality') {
              city = data;
            } else if (type == 'administrative_area_level_1') {
              state = data;
            } else if (type == 'country') {
              country = data;
            }
          }
        }


        var result = {
          fullName: parsed.result.formatted_address,
          placeId: parsed.result.place_id,
          city: city,
          state: state,
          country: country,
          lat: parsed.result.geometry.location.lat,
          long: parsed.result.geometry.location.lng
        };

        res.contentType('application/json');
        res.send(JSON.stringify(result));
      }
      catch (err) {
        return next(err);
      }
    });

    response.on('error', function (e) {
      console.log("error connecting" + e.message);
    });
  });
});

/* GET airports details. */
router.get('/airports/:lat/:long', function(req, res, next) {

  var lat = req.params.lat;
  var long = req.params.long;

  return https.get({
    host: 'api.sandbox.amadeus.com',
    path: '/v1.2/airports/nearest-relevant?apikey=' + amadeusApiKey + '&latitude=' + lat + '&longitude=' + long
  }, function(response) {
    // Continuously update stream with data
    var body = '';
    response.on('data', function(d) {
      body += d;
    });
    // Data reception is done, do whatever with it!
    response.on('end', function() {
      try{
        var parsed = JSON.parse(body);
        var result = [];

        for(var airport in parsed){
          if (parsed.hasOwnProperty(airport)) {
            var airportDetails = parsed[airport];
            result.push({
              name: airportDetails.airport_name,
              city: airportDetails.city_name,
              code: airportDetails.airport,
              distance: airportDetails.distance,
            });
          }
        }


        res.contentType('application/json');
        res.send(JSON.stringify(result));
      }
      catch (err) {
        return next(err);
      }
    });
    response.on('error', function (e) {
      console.log("error connecting" + e.message);
    });
  });
});

module.exports = router;
