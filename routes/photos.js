var express = require('express');
var http = require('http');
var https = require('https');

var router = express.Router();

var flickrApiKey = "542a47f5d97d09ae22d979790d681132";

/* GET Photo list. */
router.get('/search/:query/:number', function (req, res, next) {

    var query = req.params.query;
    var number = req.params.number;

    query = encodeURI(query);

    var flickr = {
        method: 'flickr.photos.search',
        api_key: flickrApiKey,
        format: "json",
        nojsoncallback: 1
    };

    var str = 'method=' + flickr.method +
        '&api_key=' + flickr.api_key +
        '&tags=' + query +
        '&per_page=' + number +
        '&format=' + flickr.format +
        '&nojsoncallback=' + flickr.nojsoncallback;

    return https.get({
        host: 'api.flickr.com',
        port: 443,
        path: '/services/rest/?' + str
    }, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        // Data reception is done, do whatever with it!
        response.on('end', function() {
            try{
                var parsed = JSON.parse(body);
                var resultData  = [];

                for (var i=0; i < parsed.photos.photo.length; i++) {
                    photo = parsed.photos.photo[i];
                    t_url = "https://farm" + photo.farm + ".staticflickr.com/" + photo.server +
                        "/" + photo.id + "_" + photo.secret + ".jpg";
                    p_url = "https://www.flickr.com/photos/" + photo.owner + "/" + photo.id;

                    var photo ={
                        title: photo.title,
                        href: p_url,
                        src:t_url,
                    }
                    resultData.push(photo);
                }

                res.contentType('application/json');
                res.send(JSON.stringify(resultData));
            }
            catch (err) {
                return next(err);
            }
        });
        response.on('error', function (e) {
            console.log("error connecting" + e.message);
        });
    });
});
module.exports = router;