var express = require('express');
var http = require('http');
var https = require('https');

var router = express.Router();

var amadeusApiKey = "MTBCRay1xLJgmeAP21Au9IBVovvGHNR6";

/* GET flights list. */
router.get('/:lat/:long/:checkInDate/:checkOutDate', function(req, res, next) {

    var lat = req.params.lat;
    var long = req.params.long;

    var checkInDate = new Date(req.params.checkInDate);
    var dd = checkInDate.getDate();
    var mm = checkInDate.getMonth()+1; //January is 0!
    var yyyy = checkInDate.getFullYear();

    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    var checkInDate = yyyy+'-'+mm+'-'+dd;


    var checkOutDate = new Date(req.params.checkOutDate);
    dd = checkOutDate.getDate();
    mm = checkOutDate.getMonth()+1; //January is 0!
    yyyy = checkOutDate.getFullYear();

    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    var checkOutDate = yyyy+'-'+mm+'-'+dd;

    return https.get({
        host: 'api.sandbox.amadeus.com',
        // path: 'v1.2/hotels/search-circle?apikey=' + amadeusApiKey + '&currency=AUD&latitude=' + lat + '&longitude=' + long + '&check_in=' + checkInDate + '&check_out=' + checkOutDate
        path: '/v1.2/hotels/search-circle?apikey=' + amadeusApiKey + '&radius=50&currency=AUD&latitude=' + lat + '&longitude=' + long + '&check_in=' + checkInDate + '&check_out=' + checkOutDate
    }, function(response) {
        // Continuously update stream with data
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        // Data reception is done, do whatever with it!
        response.on('end', function() {
            try{
                var parsed = JSON.parse(body);
                var resultData = [];

                // For each itineraries in result
                for(var result in parsed.results){
                    if (parsed.results.hasOwnProperty(result)) {
                        var resultObj = parsed.results[result];
                        var resultRes = {
                            name: resultObj.property_name,
                            street: resultObj.address.line1,
                            city: resultObj.address.city,
                            state: resultObj.address.region,
                            country: resultObj.address.country,
                            price: resultObj.total_price.amount
                        };

                        resultData.push(resultRes);
                    }
                }

                res.contentType('application/json');
                res.send(JSON.stringify(resultData));
            }
            catch (err) {
                return next(err);
            }
        });
        response.on('error', function (e) {
            console.log("error connecting" + e.message);
        });
    });
});

module.exports = router;
